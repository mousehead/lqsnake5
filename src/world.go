package main

import (
	"log"
	"math/rand"
)

const (
	nTilesX = 37
	nTilesY = 23

	separatorGapX    = 5
	separatorHeightY = 3

	hpPerApple = 80
)

type appleStruct struct {
	IntPoint
}

type worldState struct {
	active bool

	frame int // number of the current frame since the start
	score int // score of the player

	HP           int
	hpLossPeriod int // reciprocal velocity of HP running out

	snake *lqSnake

	apples          []appleStruct
	nApplesExpected int

	bricks []IntPoint

	freeTiles map[IntPoint]bool // works as a set, should not contain false values
}

var world worldState

func init() {
	world = worldState{
		active:       true,
		frame:        0,
		score:        0,
		HP:           400,
		hpLossPeriod: 2,

		snake: &lqSnake{
			segments: []segmentStruct{
				segmentStruct{IntPoint{X: 5, Y: 5}, segmentStraight},
				segmentStruct{IntPoint{X: 6, Y: 5}, segmentStraight},
				segmentStruct{IntPoint{X: 7, Y: 5}, segmentStraight},
			},
			direction: snakeDirectionR,
			rVelocity: 7,
		},
		apples: []appleStruct{
			{IntPoint{10, 10}},
			{IntPoint{17, 4}},
		},
		nApplesExpected: 2,

		bricks: make([]IntPoint, 128),
	}

	for x := 0; x < nTilesX; x++ {
		world.bricks = append(world.bricks, IntPoint{x, 0}, IntPoint{x, nTilesY - 1})
	}
	for y := 1; y < nTilesY-1; y++ {
		world.bricks = append(world.bricks, IntPoint{0, y}, IntPoint{nTilesX - 1, y})
	}

	middleY := nTilesY / 2
	for x := 1; x < nTilesX-2-separatorGapX; x++ {
		world.bricks = append(world.bricks, IntPoint{x, middleY})
	}

	for y := middleY - separatorHeightY; y <= middleY+separatorHeightY; y++ {
		world.bricks = append(world.bricks, IntPoint{nTilesX - 2 - separatorGapX, y})
	}

}

func spawnApples() {
	if len(world.apples) == world.nApplesExpected {
		return
	}
	newApple := appleStruct{}
	newApple.IntPoint = occupyRandomFreeTile()
	world.apples = append(world.apples, newApple)
}

func checkApples() {
	snake := world.snake
	for i := len(world.apples) - 1; i >= 0; i-- {
		a := world.apples[i]
		if a.X == snake.Head().X && a.Y == snake.Head().Y {
			snake.digesting++
			world.score++
			world.HP += hpPerApple
			world.apples = append(world.apples[:i], world.apples[i+1:]...)
		}
	}
}

func recalculateFreeTiles() {
	newFreeTiles := make(map[IntPoint]bool)
	for x := 0; x < nTilesX; x++ {
		for y := 0; y < nTilesY; y++ {
			newFreeTiles[IntPoint{x, y}] = true
		}
	}
	for _, seg := range world.snake.segments {
		delete(newFreeTiles, seg.IntPoint)
	}
	for _, a := range world.apples {
		delete(newFreeTiles, a.IntPoint)
	}
	for _, b := range world.bricks {
		delete(newFreeTiles, b)
	}
	world.freeTiles = newFreeTiles
}

// occupyRandomFreeTile returns a random tile from free tiles
// and marks it as non-free
func occupyRandomFreeTile() IntPoint {
	nFree := len(world.freeTiles)
	if nFree == 0 {
		log.Fatal("0 free tiles")
	}
	randomI := rand.Intn(nFree)
	i := 0
	for k, v := range world.freeTiles {
		if !v {
			log.Fatal("freeTiles set is corrupt")
		}
		if i == randomI {
			delete(world.freeTiles, k)
			return k
		}
		i++
	}
	log.Fatal("should have not reached here")
	return IntPoint{}
}

func isGameOver() bool {
	if world.HP <= 0 {
		return true
	}
	head := world.snake.Head()
	if head.X < 0 || head.X >= nTilesX ||
		head.Y < 0 || head.Y >= nTilesY {
		return true
	}
	for _, brick := range world.bricks {
		if head.IntPoint == brick {
			return true
		}
	}
	for _, seg := range world.snake.segments[:len(world.snake.segments)-1] {
		if head.IntPoint == seg.IntPoint {
			return true
		}
	}
	return false
}

func updateWorld() error {
	if !world.active {
		return nil
	}

	if isGameOver() {
		world.active = false
		return nil
	}

	world.frame++

	snake := world.snake
	snake.CheckArrowControls()

	if world.frame%world.hpLossPeriod == 0 {
		world.HP--
	}

	if world.frame%snake.rVelocity != 0 {
		return nil
	}

	nextDirection := akQueue.Pop()
	if nextDirection == snakeDirectionNone {
		nextDirection = snake.direction
	}

	head := snake.Head()
	newSegment := segmentStruct{IntPoint{X: head.X, Y: head.Y}, segmentStraight}
	switch nextDirection {
	case snakeDirectionU:
		newSegment.Y--
		newSegment.orientation = segmentHeadU
	case snakeDirectionD:
		newSegment.Y++
		newSegment.orientation = segmentHeadD
	case snakeDirectionL:
		newSegment.X--
		newSegment.orientation = segmentHeadL
	case snakeDirectionR:
		newSegment.X++
		newSegment.orientation = segmentHeadR
	default:
		log.Fatalf("invalid direction: %v", snake.direction)
	}

	phOrientataion := getPostHeadOrientation(snake.direction, nextDirection)
	snake.segments[len(snake.segments)-1].orientation = phOrientataion

	// a new head is appended, current head becomes body
	snake.segments = append(snake.segments, newSegment)
	if snake.digesting == 0 {
		// if stomach is empty, the tail segment is deleted
		snake.segments = snake.segments[1:]
	} else {
		snake.digesting--
	}

	snake.direction = nextDirection

	recalculateFreeTiles()
	checkApples()
	spawnApples()

	return nil
}
