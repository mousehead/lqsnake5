package main

import (
	"log"

	eb "github.com/hajimehoshi/ebiten"
)

func updateWorldAndScreen(scr *eb.Image) error {
	if err := updateWorld(); err != nil {
		return err
	}
	if eb.IsDrawingSkipped() {
		return nil
	}
	if err := updateScreen(scr); err != nil {
		return err
	}
	return nil
}

func main() {
	err := eb.Run(updateWorldAndScreen, windowWidth, windowHeight, 2, windowHeader)
	if err != nil {
		log.Fatal(err)
	}
}
