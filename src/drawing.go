package main

import (
	"fmt"
	"image/color"

	eb "github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

const (
	tileSize    = 10
	tileOffsetX = 0
	tileOffsetY = 20

	windowWidth  = 370
	windowHeight = 270

	bottomTextLineY = 252
)

func drawTile(scr *eb.Image, tileAsset *eb.Image, tileX int, tileY int) {
	xShift := float64(tileX*tileSize + tileOffsetX)
	yShift := float64(tileY*tileSize + tileOffsetY)

	op := &eb.DrawImageOptions{}
	op.GeoM.Translate(xShift, yShift)
	// in current ebiten api, DrawImage always returns a nil error
	_ = scr.DrawImage(tileAsset, op)
}

func (a appleStruct) Draw(scr *eb.Image) {
	drawTile(scr, assetImages["apple_default"], a.X, a.Y)
}

func drawFreeTiles(scr *eb.Image) {
	for tile := range world.freeTiles {
		drawTile(scr, assetImages["free_tile_default"], tile.X, tile.Y)
	}
}

func drawBricks(scr *eb.Image) {
	for _, brick := range world.bricks {
		drawTile(scr, assetImages["brick_default"], brick.X, brick.Y)
	}
}

func updateScreen(scr *eb.Image) error {
	scr.Fill(color.NRGBA{16, 16, 16, 255})

	drawFreeTiles(scr)
	drawBricks(scr)

	world.snake.Draw(scr)
	for _, apple := range world.apples {
		apple.Draw(scr)
	}

	s := fmt.Sprintf("Score: %3d  HP: %5d", world.score, world.HP)
	if !world.active {
		s += "  Game over!"
	}
	ebitenutil.DebugPrintAt(scr, s, 2, 2)

	s = fmt.Sprintf("v.%v", projectVersion)
	ebitenutil.DebugPrintAt(scr, s, 2, bottomTextLineY)
	return nil
}
