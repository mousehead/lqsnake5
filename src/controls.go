package main

import (
	eb "github.com/hajimehoshi/ebiten"
)

const (
	arrowKeysMaxQueueSize = 2
)

// so that the player can queue consecutive turns
type arrowKeysQueue struct {
	directions []snakeDirection
}

func (akq arrowKeysQueue) Len() int {
	return len(akq.directions)
}

func (akq arrowKeysQueue) Last() snakeDirection {
	if akq.Len() == 0 {
		return snakeDirectionNone
	}
	return akq.directions[len(akq.directions)-1]
}

func (akq arrowKeysQueue) Has(d snakeDirection) bool {
	for _, direction := range akq.directions {
		if direction == d {
			return true
		}
	}
	return false
}

func (akq *arrowKeysQueue) Push(d snakeDirection) {
	if akq.Len() >= arrowKeysMaxQueueSize {
		return
	}
	akq.directions = append(akq.directions, d)
}

func (akq *arrowKeysQueue) Pop() snakeDirection {
	if akq.Len() == 0 {
		return snakeDirectionNone
	}
	nextDir := akq.directions[0]
	akq.directions = akq.directions[1:]
	return nextDir
}

var akQueue = arrowKeysQueue{}

var keyboardStateKeys = [...]eb.Key{
	eb.KeyUp,
	eb.KeyDown,
	eb.KeyLeft,
	eb.KeyRight,
}

type keyboardState struct {
	pressed map[eb.Key]bool
}

func (kbs *keyboardState) Update() {
	for _, key := range keyboardStateKeys {
		kbs.pressed[key] = eb.IsKeyPressed(key)
	}
}

var lastKbdState = keyboardState{
	pressed: make(map[eb.Key]bool),
}

func (snk *lqSnake) CheckArrowKey(key eb.Key, resultDirection snakeDirection) {
	lastDirection := akQueue.Last()
	if lastDirection == snakeDirectionNone {
		lastDirection = snk.direction
	}
	if eb.IsKeyPressed(key) && // if the key is pressed
		!lastKbdState.pressed[key] && // only if it's JUST pressed (to avoid duplicates)
		resultDirection != lastDirection && // if it's not an attempt to go forward
		resultDirection != -lastDirection { // or backward
		akQueue.Push(resultDirection) // then it's truly a turn
	}
}

func (snk *lqSnake) CheckArrowControls() error {
	snk.CheckArrowKey(eb.KeyUp, snakeDirectionU)
	snk.CheckArrowKey(eb.KeyDown, snakeDirectionD)
	snk.CheckArrowKey(eb.KeyLeft, snakeDirectionL)
	snk.CheckArrowKey(eb.KeyRight, snakeDirectionR)
	lastKbdState.Update()
	return nil
}
