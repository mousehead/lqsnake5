package main

import (
	"log"
	"math"

	eb "github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
)

var assetImages = make(map[string]*eb.Image)

func loadAssetImageRotate(assetName string, fpath string, rotateDegree int) {
	rotateRadian := float64(rotateDegree) / 180 * math.Pi
	img, _, err := ebitenutil.NewImageFromFile(fpath, eb.FilterDefault)
	if err != nil {
		log.Fatal(err)
	}
	if rotateDegree == 0 {
		assetImages[assetName] = img
	} else {
		w := img.Bounds().Dx()
		h := img.Bounds().Dy()
		// Error returned by NewImage is always nil as of 1.5.0-alpha.
		img2, _ := eb.NewImage(w, h, eb.FilterDefault)
		op := &eb.DrawImageOptions{}
		op.GeoM.Translate(-float64(w)/2, -float64(h)/2)
		op.GeoM.Rotate(rotateRadian)
		op.GeoM.Translate(+float64(w)/2, +float64(h)/2)
		// in current ebiten api, DrawImage always returns a nil error
		_ = img2.DrawImage(img, op)
		assetImages[assetName] = img2
	}
}

func loadAssetImage(assetName string, fpath string) {
	loadAssetImageRotate(assetName, fpath, 0)
}

func init() {
	loadAssetImage("segment_straight", "assets/images/segment_straight.png")

	loadAssetImageRotate("segment_head_u", "assets/images/segment_head_u.png", 0)
	loadAssetImageRotate("segment_head_r", "assets/images/segment_head_u.png", 90)
	loadAssetImageRotate("segment_head_d", "assets/images/segment_head_u.png", 180)
	loadAssetImageRotate("segment_head_l", "assets/images/segment_head_u.png", 270)

	loadAssetImageRotate("segment_corner_ul", "assets/images/segment_corner_ul.png", 0)
	loadAssetImageRotate("segment_corner_ur", "assets/images/segment_corner_ul.png", 90)
	loadAssetImageRotate("segment_corner_dr", "assets/images/segment_corner_ul.png", 180)
	loadAssetImageRotate("segment_corner_dl", "assets/images/segment_corner_ul.png", 270)

	loadAssetImage("apple_default", "assets/images/apple_default.png")
	loadAssetImage("brick_default", "assets/images/brick_default.png")
	loadAssetImage("free_tile_default", "assets/images/free_tile_default.png")

}
