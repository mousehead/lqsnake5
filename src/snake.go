package main

import (
	"log"

	eb "github.com/hajimehoshi/ebiten"
)

type segmentOrientation int
type snakeDirection int

const (
	segmentStraight segmentOrientation = iota

	segmentHeadU // Up etc.
	segmentHeadD
	segmentHeadL
	segmentHeadR

	segmentCornerUL // UpLeft etc.
	segmentCornerUR
	segmentCornerDR
	segmentCornerDL
)

// note: the opposite side of direction Z is -Z
const (
	snakeDirectionNone snakeDirection = 0

	snakeDirectionU snakeDirection = -1
	snakeDirectionD snakeDirection = 1
	snakeDirectionL snakeDirection = -2
	snakeDirectionR snakeDirection = 2
)

type segmentStruct struct {
	IntPoint
	orientation segmentOrientation
}

type lqSnake struct {
	segments []segmentStruct // last is the head

	direction snakeDirection
	rVelocity int // reciprocal velocity of the snake

	digesting int // number of apples digesting
	// (will increase length when digested, can't digest more than 1 per move)

}

var segmentAssetMap = map[segmentOrientation]string{
	segmentStraight: "segment_straight",

	segmentHeadU: "segment_head_u",
	segmentHeadD: "segment_head_d",
	segmentHeadL: "segment_head_l",
	segmentHeadR: "segment_head_r",

	segmentCornerUL: "segment_corner_ul",
	segmentCornerUR: "segment_corner_ur",
	segmentCornerDR: "segment_corner_dr",
	segmentCornerDL: "segment_corner_dl",
}

// getPostHeadOrientation returns the orientation of the
// soon-to-become 2-th segment of the snake given her old direction
// and her new direction
func getPostHeadOrientation(oldD, newD snakeDirection) segmentOrientation {
	if oldD == newD {
		return segmentStraight
	}
	prodD := oldD * newD // for corners is either -2 or +2
	if prodD == 2 {
		switch oldD {
		case snakeDirectionR:
			return segmentCornerUR
		case snakeDirectionU:
			return segmentCornerUR
		default:
			return segmentCornerDL
		}
	}
	if prodD == -2 {
		switch oldD {
		case snakeDirectionU:
			return segmentCornerUL
		case snakeDirectionL:
			return segmentCornerUL
		default:
			return segmentCornerDR
		}
	}
	log.Fatal("unsatifiable pair of the old and new snake directions")
	return segmentStraight
}

func (seg segmentStruct) Draw(scr *eb.Image) error {
	assetName, ok := segmentAssetMap[seg.orientation]
	if !ok {
		log.Fatalf("no asset for orientation %v", seg.orientation)
	}
	drawTile(scr, assetImages[assetName], seg.X, seg.Y)
	return nil
}

func (snk lqSnake) Draw(scr *eb.Image) error {
	for _, seg := range snk.segments {
		err := seg.Draw(scr)
		if err != nil {
			return err
		}
	}
	return nil
}

func (snk lqSnake) Head() segmentStruct {
	return snk.segments[len(snk.segments)-1]
}
