package main

import (
	"log"
	"math/rand"
)

// IntPoint is a pair of integer coordinates
type IntPoint struct {
	X int
	Y int
}

// RandIntBetween returns a random int between a and b, both inclusive
func RandIntBetween(a, b int) int {
	if a > b {
		log.Fatal("incorrect range for RandIntBetween")
	}
	x := rand.Intn(b - a + 1)
	return x + a
}
